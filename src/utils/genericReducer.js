export const ACTION_RESET_SUFFIX = ':clear';

export function createActionReducer(myAction) {
    const myActionReset = myAction + ACTION_RESET_SUFFIX;
    const initialState = {
        actionId: undefined,
        result: undefined,
        error: undefined,
        working: false
    };

    return function reducer(state = initialState, action) {
        switch (action.type) {
            case myAction: {

                if(myAction ===  "user:profile") {
                    let profileData = { ...JSON.parse(localStorage.getItem(action.type))};
                    let actualData = profileData.updatedUserObj;

                    if(actualData && actualData.length) {
                        actualData.find((dataItem) => {
                            if(dataItem.id === action.userId) {
                              return action.updatedUserObj;
                            }
                            return dataItem;
                        })
                        localStorage.setItem(action.type, JSON.stringify(actualData));
                    } else {
                        localStorage.setItem(action.type, JSON.stringify([action.updatedUserObj]));
                    }
                    
                } else {
                    localStorage.setItem(action.type, JSON.stringify(action));
                }
                return JSON.parse(localStorage.getItem(action.type));
            }
            case myActionReset: {
                return localStorage.clear(action.type);
            }
            default: {
                return state;
            }
        }
    }
}