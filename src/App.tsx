import React from 'react';
import './App.css';
import AppContent from "./AppContent";
import './App.css';
import Header from './components/menu/Header'

function App() {
  return (
    <div className="App">
      <Header/>
      <AppContent/>
    </div>
  );
}

export default App;
