import { combineReducers } from 'redux'
import { UPDATE_USER, ACCOUNT_LOGIN } from '../actions/user'
import { createActionReducer } from '../utils/genericReducer'


export const userReducer = combineReducers({
  currentUser: createActionReducer(ACCOUNT_LOGIN),
  storedUsersProfile: createActionReducer(UPDATE_USER)
})
