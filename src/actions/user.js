// import {ACTION_RESET_SUFFIX} from '../utils/genericReducer'
const PREFIX = 'user';
export const ACCOUNT_LOGIN = PREFIX + ':login';
export const ACCOUNT_CREATE = PREFIX + ':signup';
export const ACCOUNT_LOGOUT = PREFIX + ':logout';
export const GET_USER = PREFIX + ':get';
export const UPDATE_USER = PREFIX + ':profile';

export function setLogin(email, password) {
    return {
        type: ACCOUNT_LOGIN,
        email,
        password 
    }
}
export function setSignUp(email, password ) {
    return {
        type: ACCOUNT_CREATE,
        email,
        password 
    }
}
export function setLogout() {
    return {
        type: ACCOUNT_LOGOUT
    }
}

export function getUser(userId, userToken) {
    return {
        type: GET_USER,
        userId,
        userToken
    }
}

export function updateUser(userId, updatedUserObj) {
    return {
        type: UPDATE_USER,
        userId,
        updatedUserObj
    }
}