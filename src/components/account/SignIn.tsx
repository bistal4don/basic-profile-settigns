import React, { useState, useEffect } from 'react';
import { Typography, Button, FormControl, TextField, Divider } from '@material-ui/core';
import { colors } from '../../utils/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';
import withWidth from '@material-ui/core/withWidth';
import { useSelector, useDispatch } from 'react-redux';
import { setLogin } from '../../actions/user';
import { useHistory } from 'react-router-dom';

const styles = makeStyles((theme) => ({
	formControl: {
		border: `${colors.neutral.mist} 1px solid`,
		width: 360,
		borderRadius: 5,
		marginBottom: theme.spacing(5),
		padding: theme.spacing(2),
		[theme.breakpoints.down('md')]: {
			width: 450,
		},
		[theme.breakpoints.down('xs')]: {
			width: '80%',
		},
	},
	textField: {
		textAlign: 'left',
		fontWeight: 'bold',
		color: colors.neutral.charcoal,
		marginTop: theme.spacing(2),
	},
	textFieldInput: {
		'& div': {
			borderColor: colors.neutral.rock,
		},
		'& p': {
			marginLeft: 0,
		},
	},
	forgotTextField: {
		color: colors.status.info,
		'&:hover': {
			cursor: 'pointer',
			textDecoration: 'underline',
		},
		[theme.breakpoints.down('md')]: {
			textDecoration: 'underline',
		},
	},
	button: {
		margin: '32px 0 10px 0',
		color: 'white',
		textTransform: 'capitalize',
		fontSize: '14px',
		[theme.breakpoints.down('xs')]: {
			width: '100%',
		},
	}
}));

export const SignIn = ({ width }: any) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const user = useSelector((state: any) => state.user.currentUser);
	const [validated, setValidated] = useState(false);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const classes = styles();

	useEffect(() => {
		if (user && user.email) {
			history.push('/');
		}
	}, [user, history]);

	useEffect(() => {
			
		return () => {
		clearTimeout(3000);
		}
	}, [validated]);

	const handleEmailChange = (e: any) => {
		setEmail(e.target.value);
	};

	const handlePasswordChange = (e: any) => {
		setPassword(e.target.value);
	};

	const handleSubmit = () => {
		if (!email || !password) {
			setValidated(true);
		} else {
			dispatch(setLogin(email, password));
		}
	};

	return (
		<div>

			<FormControl className={classes.formControl}>
				<Typography variant='h1' style={{ fontWeight: 'bold', textAlign: 'left' }}>
					Sign in
				</Typography>
				<Typography variant='caption' className={classes.textField}>
					Email address
				</Typography>
				<TextField
					variant='outlined'
					margin='dense'
					className={classes.textFieldInput}
					onChange={handleEmailChange}
					value={email}
					name='email'
					error={!email && validated}
					helperText={
						!email &&
						validated && (
							<span style={{ display: 'flex', alignItems: 'center', fontSize: 9 }}>
								<Typography component='span' style={{ fontSize: 11 }}>
									{' '}
									&nbsp; Enter your e-mail address{' '}
								</Typography>
							</span>
						)
					}
				/>

				<div style={{ justifyContent: 'space-between', display: 'flex', alignItems: 'baseline' }}>
					<Typography variant='caption' className={classes.textField}>
						Password
					</Typography>
					<Typography variant='caption' className={classes.forgotTextField}>
						Forgot Password
					</Typography>
				</div>
				<TextField
					variant='outlined'
					margin='dense'
					className={classes.textFieldInput}
					onChange={handlePasswordChange}
					value={password}
					name='description'
					type="password"
					error={!password && validated}
					helperText={
						!password &&
						validated && (
							<span style={{ display: 'flex', alignItems: 'center', fontSize: 9 }}>
								<Typography component='span' style={{ fontSize: 11 }}>
									{' '}
									&nbsp; Enter your password{' '}
								</Typography>
							</span>
						)
					}
				/>

				<Button variant='contained' className={classes.button} color='primary' onClick={handleSubmit}>
					Login
				</Button>

				<Typography variant='caption' style={{ textAlign: 'left', marginTop: 8, marginBottom: 8, color: colors.neutral.charcoal }}>
					By signing-in you agree with our Conditions of Use & Sale.
				</Typography>

				<Divider />
			</FormControl>
		</div>
	);
};

export default withWidth()(SignIn);