import React, { Fragment } from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import { Typography, Tab, Tabs, Menu, MenuItem } from '@material-ui/core';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { colors } from '../../utils/styles';
import {MenuiItemObj} from '../../interface/Menu'

const styles = makeStyles((theme) => ({
	content: {
		maxWidth: '100%',
		margin: '0 auto',
		overflow: 'hidden',
		paddingLeft: theme.spacing(3),
		paddingRight: theme.spacing(3),
		boxSizing: 'border-box',
		boxShadow: '2px 2px 3px 0 rgba(0, 0, 0, 0.2)',
		marginBottom: theme.spacing(0.25),
		[theme.breakpoints.down('md')]: {
			paddingLeft: theme.spacing(6),
			paddingRight: theme.spacing(6),
		},
		[theme.breakpoints.down('sm')]: {
			paddingLeft: theme.spacing(5),
			paddingRight: theme.spacing(5),
		},
		[theme.breakpoints.down('xs')]: {
			paddingLeft: theme.spacing(1.5),
			paddingRight: theme.spacing(1.5),
		},
	},
	header: {
		display: 'flex',
		alignItems: 'center',
	},
	right: {
		minWidth: 380,
		display: 'flex',
		alignItems: 'center',
		flexWrap: 'nowrap',
		justifyContent: 'flex-end',
		[theme.breakpoints.down('sm')]: {
			width: '40%',
			minWidth: 0,
		},
		'&:hover': {
			cursor: 'pointer',
		},
	},
	logo: {
		fontSize: 20,
		fontFamily: 'SinhalaMN-Bold, Sinhala MN',
		fontWeight: 'bold',
		color: colors.primary.main,
		textDecoration: 'none',
		cursor: 'pointer',
		width: 80,
		[theme.breakpoints.down('xs')]: {
			width: 'auto',
		},
	},
	tabs: {
		textTransform: 'capitalize',
	},
	menuTabs: {
		flexGrow: 1,
	},
	tabRoot: {
		paddingTop: 6,
		marginLeft: 4,
		minWidth: 0,
		opacity: 1,
		...theme.typography.body1,
		'&:hover': {
			color: colors.primary.main,
		},
		'&:hover:not($tabSelected), &:focus': {
			boxShadow: 'inset 0 -2px 0 0 ' + colors.primary.main,
			color: colors.primary.main,
		},
		color: colors.neutral.charcoal,
		fontSize: 16,
		letterSpacing: '0.4px',
		minHeight: 50,
		lineHeight: 1.33,
	},
	tabSelected: {
		...theme.typography.body2,
		color: colors.primary.main,
		borderLeft: 0,
		fontSize: 16,
		fontWeight: "normal",
	},
	navItem: {
		fontWeight: 'bold',
		marginLeft: 20,
		'&:focus, &:hover': {
			color: colors.primary.main,
		},
	},
}));

export const menutItems: MenuiItemObj[] = [
	{
		key: '/profile',
		label: 'Profile',
		labelTitle: 'User Profile',
	},
	{
		key: '/menu2',
		label: 'Menu 2',
		labelTitle: 'menu2',
	},
	{
		key: '/menu3',
		label: 'Menu 3',
		labelTitle: 'menu3',
	},
];

export const Header = ({ width }: any) => {
	const classes = styles();
	const history = useHistory();
	const location = useLocation();
	const [value, setValue] = React.useState(location.pathname);
	const [accountAnchorEl, setAccountAnchorEl] = React.useState(null);

	const handleAccountClick = (event: any) => {
		setAccountAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAccountAnchorEl(null);
	};

	const handleChange = (event: any, newValue: any) => {

		setValue(newValue);
		history.push(newValue);
	};

	if (location.pathname.includes('/account/login')) {
		return null;
	}

	let actualValue = false;
	const findValue = menutItems.find(item => item.key === value);
	if(findValue) {
		actualValue = findValue.key;
	};

	return (
		<Fragment>
			<div className={classes.content}>
				<div className={classes.header}>
					<Link to='/' className={classes.logo} onClick={() => setValue('')}>
						Logo
					</Link>

					<div style={{ width: '85%' }}>
						<Tabs
							className={classes.menuTabs}
							value={actualValue}
							onChange={handleChange}
							aria-label='disabled tabs example'
						>
							{menutItems.map((item, index) => {
								return (
									<Tab
										label={item.label}
										value={item.key}
										className={classes.tabs}
										classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
										key={index}
									/>
								);
							})}
						</Tabs>
					</div>
					<div className={classes.right}>
						{isWidthUp('sm', width) && (
							<Typography variant='caption' className={classes.navItem} onClick={handleAccountClick}>
								Settings
							</Typography>
						)}
					</div>
					<Menu
						id='simple-menu'
						anchorEl={accountAnchorEl}
						keepMounted
						open={Boolean(accountAnchorEl)}
						getContentAnchorEl={null}
						anchorOrigin={{
							vertical: 'bottom',
							horizontal: 'center',
						}}
						transformOrigin={{
							vertical: 'top',
							horizontal: 'center',
						}}
						onClose={handleClose}
					>
						<MenuItem onClick={handleClose}>My account</MenuItem>
						<MenuItem onClick={handleClose}>Login Settings</MenuItem>
						<MenuItem onClick={handleClose}>Logout</MenuItem>
					</Menu>
				</div>
			</div>
		</Fragment>
	);
}

export default withWidth()(Header);
