import React from "react";
import withWidth from "@material-ui/core/withWidth";
import SignIn from "./components/account/SignIn";
import HomePage from "./pages/HomePage";
import NotFound from "./pages/NotFound";
import ProfileList from "./pages/profile/ProfileList";
import ProfileEdit from "./pages/profile/ProfileEdit";
import { Switch, Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

export function AppContent({ width }: any) {
  return (
    <Switch>
      <Route exact path="/account/login" key="/account/login" component={SignIn} />
      <ProtectedRoute exact path="/" key="/" component={HomePage} />
      <ProtectedRoute exact path="/profile" key="/profile" component={ProfileList} />
      <ProtectedRoute exact path="/profile/:profileId" key="/profileId" component={ProfileEdit} />
      <Route key="notFound" component={NotFound}/>
    </Switch>
  );
}

// this component is for routes that can be accessed only upon login
const ProtectedRoute = ({ component: Component, ...rest }: any) => {
  const currentUser = useSelector((state: any) => state.user.currentUser);

  const isLoggedIn = !!currentUser.email || localStorage.getItem("user:login");

  return <Route {...rest} render={() => (isLoggedIn ? <Component /> : <Redirect to="/account/login" />)} />;
};

export default withWidth()(AppContent);
