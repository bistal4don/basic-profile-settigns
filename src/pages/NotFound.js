import React from 'react';
import { Typography, Divider } from '@material-ui/core';
import { colors } from '../utils/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { ReactComponent as ErrorIcon } from '../icons/error.svg'

const styles = makeStyles((theme) => ({
	notFoundPage: {
        backgroundColor: colors.neutral.cloud,
        border: 'solid 1px ' + colors.status.danger,
        borderRadius: 4,
        width: 350,
        margin: '20px auto',
		[theme.breakpoints.down('xs')]: {
			width: '100%',
			height: 'auto',
		},
	},
	notFoundContent: {
        display: 'flex',
        padding: '20px 10px',
		textAlign: 'left',
		[theme.breakpoints.down('xs')]: {
			width: '95%',
		},
	},
}));

export const NotFound = () => {
	const classes = styles();

	return (
		<div className={classes.notFoundPage}>
			<Divider />

			<div className={classes.notFoundContent}>

                <ErrorIcon/>
                <div style={{paddingLeft: 20}}>
				<Typography variant='body2' style={{color: colors.status.danger}} >Not Found</Typography>
				<Typography variant='body1'>
					The Page you requested was not found
				</Typography>
                </div>
			</div>
			<Divider />
		</div>
	);
};

export default NotFound;