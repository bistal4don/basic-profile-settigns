import React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import withWidth from '@material-ui/core/withWidth';
import { contentPadding } from '../utils/styles';

const styles = makeStyles((theme) => ({
	content: {
		maxWidth: '100%',
		margin: '0 auto',
		overflow: 'hidden',
		paddingLeft: contentPadding,
		paddingTop: contentPadding,
        fontSize: 28,
        fontWeight: 'bold',
		paddingRight: contentPadding,
		[theme.breakpoints.down('lg')]: {
			paddingLeft: theme.spacing(3),
			paddingRight: theme.spacing(3),
		},
		[theme.breakpoints.down('md')]: {
			paddingLeft: theme.spacing(0),
			paddingRight: theme.spacing(0),
		},
	}
}));

export function LandingPage({ width }: any) {
	const classes = styles();
	return (
		<div className={classes.content}>
                You are now logged in
		</div>
	);
}

export default withWidth()(LandingPage);