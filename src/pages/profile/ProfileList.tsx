import React, { Fragment } from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import { Button, Grid, Typography, List, ListItem } from '@material-ui/core';
import { border1, colors } from '../../utils/styles';
import { useLocation } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import { menutItems } from '../../components/menu/Header';
import usersProfile from '../../mocks/userProfile'

const styles = makeStyles((theme) => ({
	content: {
		margin: '0 auto',
		width: '80%',
		borderTop: 0,
		paddingTop: theme.spacing(4),
		[theme.breakpoints.down('sm')]: {
			width: '100%',
		},
		'& li': {
			borderBottom: '1px solid ' + colors.neutral.mist,
		},
		'& li:first-child': {
			color: colors.primary.main,
			paddingBottom: 8,
			borderBottom: '2px solid ' + colors.primary.main,
			[theme.breakpoints.down('xs')]: {
				borderTop: '1px solid ' + colors.neutral.mist,
				borderBottom: '1px solid ' + colors.neutral.mist,
			},
		},
		'& li:nth-child(even)': {
			backgroundColor: '#f6f9f5',
		},
	},
	gutters: {
		paddingTop: 4,
		paddingBottom: 4,
	},
	button: {
		height: 25,
		width: '40%',
		textTransform: 'capitalize',
		margin: '0 3px',
		fontSize: '16px',
	},
	dot: {
		cursor: 'pointer',
		height: 5,
		width: 5,
		borderRadius: '50%',
		margin: '0 1px',
		display: 'inline-block',
	},
	subTitile: {
		backgroundColor: colors.neutral.river,
		paddingLeft: theme.spacing(3),
		paddingRight: theme.spacing(3),
		height: 50,
		textAlign: 'left',
		borderBottom: border1,
		padding: 32,
		[theme.breakpoints.down('md')]: {
			paddingLeft: theme.spacing(6),
			paddingRight: theme.spacing(6),
		},
		[theme.breakpoints.down('sm')]: {
			paddingLeft: theme.spacing(5),
			paddingRight: theme.spacing(5),
		},
		[theme.breakpoints.down('xs')]: {
			paddingLeft: theme.spacing(1.5),
			paddingRight: theme.spacing(1.5),
		},
	},
}));

export function ProfileList({ width }: any) {
	const classes = styles();
	const location = useLocation();
	const history = useHistory();
	const storedUsersProfile: any[] = JSON.parse(localStorage.getItem("user:profile") || '[]') || [];
	let users: any[] =  storedUsersProfile;

	if(users && !users.length) {
		users =  usersProfile;
	}

	let pathname = location.pathname;
	const mobileView = isWidthDown('xs', width);

	return (
		<Fragment>
			<div className={classes.subTitile}>
				<Typography variant='h1' style={{ lineHeight: 0.8 }}>
					{menutItems?.find((item: any) => item?.key === pathname)?.labelTitle}
				</Typography>
				<Typography variant='caption'>List of all users profile</Typography>
			</div>
			<List className={classes.content}>
				{!mobileView ? (
					<ListItem classes={{ gutters: classes.gutters }}>
						<Grid item sm={1}>
							<Typography variant='body2'> Id</Typography>
						</Grid>
						<Grid item sm={1}>
							<Typography variant='body2'> Name</Typography>
						</Grid>
						<Grid item sm={2}>
							<Typography variant='body2'> Emaill</Typography>
						</Grid>
						<Grid item sm={4}>
							<Typography variant='body2'> Address</Typography>
						</Grid>

				
						<Grid item sm={1}>
							<Typography variant='body2'> Company</Typography>
						</Grid>
						<Grid item sm={1}>
							<Typography variant='body2'> Mobile</Typography>
						</Grid>

						<Grid item sm={2}>
							<Typography variant='body2' style={{textAlign: "center"}}> Actions</Typography>
						</Grid>
					</ListItem>
				) : (
					''
				)}
				{users?.map((item: any, index: any) => {
					return (
						<ListItem classes={{ gutters: classes.gutters }} key={index}>
							<Grid container>
								<Grid item sm={1} xs={11}>
									{mobileView && (
											<Typography variant='caption' color='textSecondary'>
												Id:
											</Typography>
										)}
									<Typography color='textPrimary' variant='caption'>
										&nbsp;{item.id}
									</Typography>
								</Grid>

								{mobileView && (
									<Grid item xs={1}>
										<span className={classes.dot} style={{ backgroundColor: colors.neutral.stone }}></span>
										<span className={classes.dot} style={{ backgroundColor: colors.neutral.stone }}></span>
										<span className={classes.dot} style={{ backgroundColor: colors.neutral.stone }}></span>
									</Grid>
								)}


								<Grid item sm={1} xs={10}>
									{mobileView && (
											<Typography variant='caption' color='textSecondary'>
												Name:
											</Typography>
										)}
									<Typography color='textPrimary' variant='caption' component="span">
										&nbsp;{item.first_name} {item.other_names} 
									</Typography>
								</Grid>

								<Grid item sm={2} xs={10}>
									{mobileView && (
										<Typography variant='caption' color='textSecondary'>
											Email:
										</Typography>
									)}
									<Typography variant='caption' color='textPrimary'>
										{item.email}
									</Typography>
								</Grid>

					
								<Grid item sm={4} xs={12}>
									{mobileView && (
										<Typography variant='caption' color='textSecondary'>
											Address:
										</Typography>
									)}

									<Typography variant='caption' color='textPrimary' component='span'>
									&nbsp; {item.address.street} &nbsp;
										{item.address.town} &nbsp;
										{item.address.country} &nbsp;
										{item.address.postcode}&nbsp;
									</Typography>
								</Grid>

								<Grid item sm={1} xs={10}>
									{mobileView && (
										<Typography variant='caption' color='textSecondary'>
											Company:
										</Typography>
									)}
									<Typography variant='caption' color='textPrimary'>
										{item.company}
									</Typography>
								</Grid>


								<Grid item sm={1} xs={10}>
									{mobileView && (
										<Typography variant='caption' color='textSecondary'>
											Mobile:
										</Typography>
									)}
									<Typography variant='caption' color='textPrimary'>
										{item.mobile}
									</Typography>
								</Grid>


								<Grid item sm={2}>
									{!mobileView && (
										<div style={{  textAlign: "center" }}>

											 <Button variant='contained' color='primary' className={classes.button} style={{ color: 'white' }} onClick={() => history.push(`/profile/${item.id}`)}>
												<Typography variant='caption'>Edit</Typography>
											</Button>
											<Button variant='outlined' color='primary' className={classes.button} onClick={() => history.push('/')}>
												<Typography variant='caption'>Cancel</Typography>
											</Button>
										</div>
									)}
								</Grid>
							</Grid>
						</ListItem>
					);
				})}
			</List>
		</Fragment>
	);
}

export default withWidth()(ProfileList);
