import React, { useState } from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import withWidth from '@material-ui/core/withWidth';
import { Button, Typography, TextField } from '@material-ui/core';
import { updateUser } from '../../actions/user';
import { useDispatch } from 'react-redux';
import { colors } from '../../utils/styles';
import { useLocation } from 'react-router-dom';
import usersProfile from '../../mocks/userProfile'

const styles = makeStyles((theme) => ({
	backgroundColor: {
		margin: '0 auto',
		textAlign: 'left',
		width: 600,
		backgroundColor: colors.neutral.river,
		borderTop: 0,
		padding: theme.spacing(4),
		[theme.breakpoints.down('xs')]: {
			width: '90%',
		},
	},
	input: {
        width: '100%',
        backgroundColor: 'white',
        marginBottom: 20,
		borderColor: colors.neutral.stone,
		'& input': {
			paddingTop: 8,
			paddingBottom: 8,
		},
	},
	left: {
        fontWeight: 'bold',
	},
	button: {
		marginTop: 15,
		height: 35,
		marginLeft: 15,
		[theme.breakpoints.down('xs')]: {
			width: 120,
		},
	},
	buttons: {
		textAlign: 'right',
		[theme.breakpoints.down('xs')]: {
			width: '90%',
		},
	},
}));

export function ProfileEdit({ width }: any) {
	const classes = styles();
	const dispatch = useDispatch();
	const location = useLocation();
	const users: any[] = usersProfile || [];

	let userId = location.pathname && location.pathname.split('/')[2];

	const filteredUser = users.find((user) => user.id === userId);

	const clonedUser = { ...filteredUser };
	const [userDetail, setUserDetails] = useState(clonedUser);
	const {first_name, company, email, mobile, other_names, address} = userDetail;
	const [userAddress, setUserAddress] = useState({...address});
	const {street, town, county, postcode} = userAddress;

	const [isDisabled, setDisabled]= useState(false);

	const handleChange = (event: any, isAddress = false) => {
        const { value, name } = event.target;
		if(isAddress) {
			setUserAddress({...userAddress, [name]: value});
		} else {
			setUserDetails({...userDetail, [name]: value});
		}
	};

	const handleSave = () => {

		let editedUser = userDetail;
		editedUser.address = userAddress;
		dispatch(updateUser(userId, editedUser));
		setDisabled(true);
	};

	return (
            <div style={{width: "40%", margin: "60px auto"}}>

				<Typography variant='h2' style={{textAlign: "left", padding: '20px 0'}} color="textPrimary">
					Personal Info
				</Typography>

           		<div style={{ display: 'flex', alignItems: 'center' }}>
					<Typography variant='caption' className={classes.left} color="textPrimary">
					First Name &nbsp;
					</Typography>
				</div>
                <TextField
                    variant='outlined'
					disabled={isDisabled}
                    className={classes.input}
                    onChange={handleChange}
                    value={first_name}
                    name='first_name'
                    placeholder={'Modify first name'}
                    />
           
		   		<div style={{ display: 'flex', alignItems: 'center' }}>
					<Typography variant='caption' className={classes.left} color="textPrimary">
					Other Name &nbsp;
					</Typography>
				</div>
                <TextField
                    variant='outlined'
					disabled={isDisabled}
                    className={classes.input}
                    onChange={handleChange}
                    value={other_names}
                    name='other_names'
                    placeholder={'Modify other name'}
                    />
		   		
				<div style={{ display: 'flex', alignItems: 'center' }}>
					<Typography variant='caption' className={classes.left} color="textPrimary">
					Other Name &nbsp;
					</Typography>
				</div>
                <TextField
                    variant='outlined'
					disabled={isDisabled}
                    className={classes.input}
                    onChange={handleChange}
                    value={email}
                    name='email'
                    placeholder={'Modify Email'}
                    />
		   		
				<div style={{ display: 'flex', alignItems: 'center' }}>
					<Typography variant='caption' className={classes.left} color="textPrimary">
					Other Name &nbsp;
					</Typography>
				</div>
                <TextField
                    variant='outlined'
					disabled={isDisabled}
                    className={classes.input}
                    onChange={handleChange}
                    value={company}
                    name='company'
                    placeholder={'Modify Company'}
                    />
		   		
				<div style={{ display: 'flex', alignItems: 'center' }}>
					<Typography variant='caption' className={classes.left} color="textPrimary">
					Other Name &nbsp;
					</Typography>
				</div>
                <TextField
                    variant='outlined'
					disabled={isDisabled}
                    className={classes.input}
                    onChange={handleChange}
                    value={mobile}
                    name='mobile'
                    placeholder={'Modify Mobile'}
                    />

				<Typography variant='h2' style={{textAlign: "left", padding: '20px 0'}} color="textPrimary">
					Address
				</Typography>

				<div style={{ display: 'flex', alignItems: 'center' }}>
					<Typography variant='caption' className={classes.left} color="textPrimary">
						Street &nbsp;
					</Typography>
				</div>
                <TextField
					disabled={isDisabled}
                    variant='outlined'
                    className={classes.input}
                    onChange={(e) => handleChange(e, true)}
                    value={street}
                    name='street'
                    placeholder={'Modify Street'}
                    />

				<div style={{ display: 'flex', alignItems: 'center' }}>
					<Typography variant='caption' className={classes.left} color="textPrimary">
						Town &nbsp;
					</Typography>
				</div>
                <TextField
                    variant='outlined'
					disabled={isDisabled}
                    className={classes.input}
                    onChange={(e) => handleChange(e, true)}
                    value={town}
                    name='town'
                    placeholder={'Modify Town'}
                    />

				<div style={{ display: 'flex', alignItems: 'center' }}>
					<Typography variant='caption' className={classes.left} color="textPrimary">
						County &nbsp;
					</Typography>
				</div>
                <TextField
                    variant='outlined'
					disabled={isDisabled}
                    className={classes.input}
                    onChange={(e) => handleChange(e, true)}
                    value={county}
                    name='county'
                    placeholder={'Modify County'}
                    />

				<div style={{ display: 'flex', alignItems: 'center' }}>
					<Typography variant='caption' className={classes.left} color="textPrimary">
						Postcode &nbsp;
					</Typography>
				</div>
                <TextField
                    variant='outlined'
                    className={classes.input}
					disabled={isDisabled}
                    onChange={(e) => handleChange(e, true)}
                    value={postcode}
                    name='postcode'
                    placeholder={'Modify Postcode'}
                    />

				<div className={classes.buttons} style={{marginBottom: 30}}>
					<Button variant='outlined' className={classes.button} href="/profile">
						{isDisabled ? 'Back' : 'Cancel' }
					</Button>
			
					{isDisabled ?  <Button variant='contained' className={classes.button} onClick={()=> setDisabled(false)} >
						 Edit
					</Button>: 		<Button variant='contained' className={classes.button} onClick={handleSave}>
						Save
					</Button>}
				</div>
            </div>
	);
}

export default withWidth()(ProfileEdit);
